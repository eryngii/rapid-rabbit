const gulp = require('gulp')
const notify = require('gulp-notify')
const plumber = require('gulp-plumber')
const watch = require('gulp-watch')
const browser = require('browser-sync')
const pug = require('gulp-pug')
const stylus = require('gulp-stylus')
const nib = require('nib')
const webpack = require('webpack')
const webpackStream = require('webpack-stream')
const webpackConfig = require('./webpack.config')
const colors = require('colors')

const plumberOption = {
  errorHandler: notify.onError('<%= error.message =%>')
}

const getRelativePath = path => path.replace(`${__dirname}/`, '')

gulp.task('browser-sync', vinyl => {
  return browser({
    notify: false,
    // proxy: 'http://localhost:8888',
    server: { baseDir: 'dist/' }
  })
})

gulp.task('watch', () => {
  watch('src/pug/**/*.pug', pug2html)
  watch('src/stylus/**/*.styl', stylus2css)
  watch('src/js/**/*.js', bundleJs)
  return null
})

const pug2html = vinyl => {
  const { path } = vinyl
  const isLayoutFile = /\/_.*\.pug$/.test(path)
  const src = isLayoutFile ? './src/pug/**/*.pug' : path
  console.log(`[${colors.red('pug')}] ${getRelativePath(path)}`)
  return gulp.src([src, '!src/pug/_*.pug'], { base: 'src/pug/' })
    .pipe(plumber(plumberOption))
    .pipe(pug())
    .pipe(gulp.dest('dist/'))
    .pipe(browser.stream())
}
gulp.task('pug', pug2html)

const stylus2css = vinyl => {
  const { path } = vinyl
  console.log(`[${colors.green('stylus')}] ${getRelativePath(path)}`)
  return gulp.src(['src/stylus/**/*.styl', '!src/stylus/**/_*.styl', '!src/stylus/**/_*/**/*'], { base: 'src/stylus/' })
    .pipe(plumber(plumberOption))
    .pipe(stylus({ use: nib() }))
    .pipe(gulp.dest('dist/'))
    .pipe(browser.stream())
}
gulp.task('stylus', stylus2css)

const bundleJs = vinyl => {
  const { path } = vinyl
  console.log(`[${colors.yellow('javascript')}] ${getRelativePath(path)}`)
  return webpackStream(webpackConfig, webpack)
    .pipe(plumber(plumberOption))
    .pipe(gulp.dest('dist/js/'))
    .pipe(browser.stream())
}
gulp.task('javascript', bundleJs)

gulp.task('default', gulp.parallel('browser-sync', 'watch'))
