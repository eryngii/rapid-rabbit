#!/usr/bin/env node

const { join } = require('path')
const { promises: { mkdir, readdir }, copy } = require('fs-extra')
const validFilename = require('valid-filename')
// const colors = require('colors')

const exit = (message, exitCode) => {
  console.error(message)
  process.exit(exitCode)
}

// const getRelativePath = path => path.replace(`${process.cwd()}/`, '')

const main = async () => {
  if (process.argv.length !== 3) {
    exit('invalid argument', 1)
  }

  const name = process.argv[2]
  if (!validFilename(name)) {
    exit('invalid project name', 1)
  }

  const projectPath = join(process.cwd(), name)
  const templatePath = join(__dirname, '..', 'template')

  await mkdir(projectPath)

  const copyFiles = (await readdir(templatePath))
    .filter(filename => filename !== 'node_modules')
    .map(filename => {
      const src = join(templatePath, filename)
      const dist = join(projectPath, filename)
      return copy(src, dist)
    })

  await Promise.all(copyFiles)

  console.log('🎉 Finish!!')
}

main()
