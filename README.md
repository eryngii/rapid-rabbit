Rapid Rabbit
====

Rapid Rabbit is an Application Generator

## Installation

```sh
$ npm install -g rapid-rabbit
```

## Usage
```sh
$ rapid-rabbit your-project
$ cd your-project
$ npm install
```
